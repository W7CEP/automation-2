
		/* ###### Clone Pokemon slot 2 ###### */
	// teleport to daycare in wildarea
	{ X,          5 },	{ NOTHING,  100 }, //open menu
	{ A,          5 },	{ NOTHING,  100 }, 
	{ A,          5 },	{ NOTHING,  100 }, //you want to teleport here?
	{ A,          5 },	{ NOTHING,  100 }, //sure!

	// walk to daycare and get an egg
	{ DOWN,      70 },	{ NOTHING,    5 }, //walk down to daycare
	{ LEFT,       5 },	{ NOTHING,    5 }, //a little bit left
	{ A,          5 },	{ NOTHING,  100 }, //talk to her "I have an egg for you, do you want it?"
 	{ A,          5 },	{ NOTHING,  200 }, //yes I do
	{ A,          5 },	{ NOTHING,  100 }, //you got it
	{ A,          5 },	{ NOTHING,  100 }, //Put egg on your team
	{ A,          5 },	{ NOTHING,  100 }, //please select the slot!
	{ DOWN,       5 },	{ NOTHING,    5 }, //select correct pokemon slot
	{ A,          5 },	{ NOTHING,  100 }, //You sure want to put it here?
	{ DOWN,       5 },	{ NOTHING,    5 }, //send to box as is
	{ A,          5 },	{ NOTHING,  200 }, //Yes!
	{ A,          5 },	{ NOTHING,  200 }, //Yes!
	{ A,          5 },	{ NOTHING,  100 }, //take good care of it

	// start hatching
	{ PLUS,       5 },	{ NOTHING,    5 }, //get on your bike
	{ POSITION,  50 },	{ NOTHING,    5 },
	{ UP,        20 },	{ NOTHING,    5 },
	{ POSITION,  60 },	{ NOTHING,    5 }, //get into position
	{ SPIN,  cycles },	{ NOTHING,    5 }, //spin for X cycles

	{ PLUS,       5 },	{ NOTHING,  100 }, //get off the bike

	/* ###### Clone Pokemon slot 3 ###### */
	// teleport to daycare in wildarea
	{ X,          5 },	{ NOTHING,  100 }, //open menu
	{ A,          5 },	{ NOTHING,  100 }, 
	{ A,          5 },	{ NOTHING,  100 }, //you want to teleport here?
	{ A,          5 },	{ NOTHING,  100 }, //sure!

	// walk to daycare and get an egg
	{ DOWN,      70 },	{ NOTHING,    5 }, //walk down to daycare
	{ LEFT,       5 },	{ NOTHING,    5 }, //a little bit left
	{ A,          5 },	{ NOTHING,  100 }, //talk to her "I have an egg for you, do you want it?"
 	{ A,          5 },	{ NOTHING,  200 }, //yes I do
	{ A,          5 },	{ NOTHING,  100 }, //you got it
	{ A,          5 },	{ NOTHING,  100 }, //Put egg on your team
	{ A,          5 },	{ NOTHING,  100 }, //please select the slot!
	{ DOWN,       5 },	{ NOTHING,    5 }, //select correct pokemon slot
	{ A,          5 },	{ NOTHING,  100 }, //You sure want to put it here?
	{ DOWN,       5 },	{ NOTHING,    5 }, //send to box as is
	{ A,          5 },	{ NOTHING,  200 }, //Yes!
	{ A,          5 },	{ NOTHING,  200 }, //Yes!	
	{ A,          5 },	{ NOTHING,  100 }, //take good care of it

	// start hatching
	{ PLUS,       5 },	{ NOTHING,    5 }, //get on your bike
	{ POSITION,  50 },	{ NOTHING,    5 },
	{ UP,        20 },	{ NOTHING,    5 },
	{ POSITION,  60 },	{ NOTHING,    5 }, //get into position
	{ SPIN,  cycles },	{ NOTHING,    5 }, //spin for X cycles

	{ PLUS,       5 },	{ NOTHING,  100 }, //get off the bike